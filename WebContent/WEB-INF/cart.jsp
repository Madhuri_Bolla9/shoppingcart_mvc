<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cart</title>
</head>
<body>
	<a href="/ShoppingCart/catalog">Back to catalog</a>
	<table width=50%>
			<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Qty</th>
			<th>Item totalPrice</th>
			<th></th>
		</tr>
		
		<c:forEach items="${cart.products}" var="cartProduct">
			<tr>
				<td>${cartProduct.product.id}</td>
				<td>${cartProduct.product.name}</td>
				<td>${cartProduct.qty}</td>
				<td>${cartProduct.price}</td>
				<td><a href="/ShoppingCart/removeFromCart?id=${cartProduct.product.id}">Remove from cart</a></td>
			</tr>
		</c:forEach>
		<tr>
			<td>Total Price:${cart.getPrice()}</td>
		</tr>
		
	</table>
</body>
</html>