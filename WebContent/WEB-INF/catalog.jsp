<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Catalog</title>
</head>
<body>
	<H1>Product Catalog</H1>
	Items in Cart: ${cartItemsCount} <a href="/ShoppingCart/showCart">Show Cart</a>
	<table width=100%>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Qty</th>
			<th>Price</th>
			<th></th>
		</tr>
		<c:forEach items="${catalog}" var="product">
		
			<tr>
				<td>${product.id}</td>
				<td>${product.name}</td>
				<td>${product.qty}</td>
				<td>${product.price}</td>
				<td>	
					<c:if test="${product.qty gt 0}">
						<a href="/ShoppingCart/addToCart?id=${product.id}">Add to cart</a>
					</c:if>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>