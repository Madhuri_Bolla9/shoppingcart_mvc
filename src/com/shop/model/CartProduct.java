package com.shop.model;

public class CartProduct {
	private Product product;
	private int qty;
	private double price;
	
	
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public double getPrice()
	{
		price= getProduct().getPrice()*qty;
		return price;
	}
	public void setPrice(double price)
	{
		this.price = price;
	}
}
