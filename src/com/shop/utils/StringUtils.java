package com.shop.utils;

public class StringUtils {
	public static boolean isNullOrEmpty(String string){
		return string == null || string.trim().equals("");
	}
}
