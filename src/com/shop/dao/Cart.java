package com.shop.dao;

import java.util.ArrayList;
import java.util.List;

import com.shop.model.CartProduct;
import com.shop.model.Product;

public class Cart {
	
	private List<CartProduct> products = new ArrayList<CartProduct>();
	
	public List<CartProduct> getProducts() {
		return products;
	}
	
	public void addToCart(Product product) {
		for(CartProduct cartProduct : products) {
			if(product.getId() == cartProduct.getProduct().getId()) {
				cartProduct.setQty(cartProduct.getQty() + 1);
				Catalog.reduceQty(product);
				return;
			}
		}
		CartProduct cartProduct = new CartProduct();
		cartProduct.setProduct(product);
		cartProduct.setQty(1);
		Catalog.reduceQty(product);
		products.add(cartProduct);
	}

	public int getItemCount() {
		int count = 0;
		for(CartProduct cartProduct : products) {
			count += cartProduct.getQty();
		}
		return count;
	}

	public void removeFromCart(Product product) {
		for(CartProduct cartProduct : products) {
			if(cartProduct.getProduct().getId() == product.getId()) {
				cartProduct.setQty(cartProduct.getQty() - 1);
				if(cartProduct.getQty() <= 0) {
					products.remove(cartProduct);
				}
				Catalog.incQty(product);
				return;
			}
		}
	}
	public double getPrice()
	{
		double totalprice = 0;
		for(CartProduct cartProduct : products) {
				totalprice+= cartProduct.getPrice();
		}
		return totalprice;
	}
}
	
