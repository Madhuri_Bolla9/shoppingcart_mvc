package com.shop.dao;

import java.util.ArrayList;
import java.util.List;

import com.shop.model.Product;

public class Catalog {
	
	private static List<Product> products = new ArrayList<Product>(){{
		for(int i = 0;i<10;i++){
			Product product = new Product();
			product.setId(i);
			product.setName("Name " + i);
			product.setQty(5);
			product.setPrice(10);
			add(product);
		}
	}};
	
	public static List<Product> getProducts() {
		return products;
	}
	
	public static synchronized void reduceQty(Product product) {
		product.setQty(product.getQty() - 1);
	}
	
	public static synchronized void incQty (Product product) {
		product.setQty(product.getQty() + 1);
	}
	
	public static synchronized Product getProductById(int id) {
		for(Product product : products) {
			if(product.getId() == id) {
				return product;
			}
		}
		return null;
	}
}
	
