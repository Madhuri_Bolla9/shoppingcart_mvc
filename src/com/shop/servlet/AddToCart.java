package com.shop.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shop.dao.Cart;
import com.shop.dao.Catalog;
import com.shop.model.Product;
import com.shop.utils.StringUtils;

public class AddToCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		if(!StringUtils.isNullOrEmpty(id)) {
			int prodId = Integer.parseInt(id);
			Product product = Catalog.getProductById(prodId);
			if(product != null) {
				Cart cart = (Cart) request.getSession().getAttribute("Cart");
				if(cart == null) {
					cart = new Cart();
					request.getSession().setAttribute("Cart", cart);
				}
				cart.addToCart(product);
			}
		}
		response.sendRedirect("/ShoppingCart/catalog");
	}

}
