package com.shop.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shop.dao.Cart;

public class ShowCartServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Cart cart = (Cart) req.getSession().getAttribute("Cart");
		if(cart== null) {
			cart = new Cart();
		}
		req.setAttribute("cart", cart);
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/cart.jsp");
		dispatcher.forward(req, resp);
	}
}
