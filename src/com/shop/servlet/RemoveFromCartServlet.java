package com.shop.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shop.dao.Cart;
import com.shop.dao.Catalog;
import com.shop.utils.StringUtils;

public class RemoveFromCartServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id = req.getParameter("id");
		if(!StringUtils.isNullOrEmpty(id)) {
			Cart cart = (Cart) req.getSession().getAttribute("Cart");
			if(cart != null) {
				int productID = Integer.parseInt(id);
				cart.removeFromCart(Catalog.getProductById(productID));
			}
		}
		resp.sendRedirect("/ShoppingCart/showCart");
	}
}
