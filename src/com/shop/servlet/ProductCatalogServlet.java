package com.shop.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shop.dao.Cart;
import com.shop.dao.Catalog;
import com.shop.model.Product;

public class ProductCatalogServlet extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Product> products = Catalog.getProducts();
		req.setAttribute("catalog", products);
		Cart cart = (Cart) req.getSession().getAttribute("Cart");
		if(cart == null) {
			req.setAttribute("cartItemsCount", 0);
		} else {
			req.setAttribute("cartItemsCount", cart.getItemCount());
		}
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/catalog.jsp");
		dispatcher.forward(req, resp);
	}
}
